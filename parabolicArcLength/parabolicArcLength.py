#!/usr/bin/python
# -*- coding: UTF-8 -*-


'''We want to approximate the length of a curve representing a function y = f(x) with a<= x <= b. First, we split the interval 
[a, b] into n sub-intervals with widths h1, h2, ... , hn by defining points x1, x2 , ... , xn-1 between a and b. 
This defines points P0, P1, P2, ... , Pn on the curve whose x-coordinates are a, x1, x2 , ... , xn-1, b and y-coordinates f(a), 
f(x1), ..., f(xn-1), f(b) . By connecting these points, we obtain a polygonal path approximating the curve.

Our task is to approximate the length of a parabolic arc representing the curve y = x * x with x in the interval [0, 1]. 
We will take a common step h between the points xi: h1, h2, ... , hn = h = 1/n and we will consider the points P0, P1, P2, ... , 
Pn on the curve. The coordinates of each Pi are (xi, yi = xi * xi).

The function len_curve (or similar in other languages) takes n as parameter (number of sub-intervals) and returns 
the length of the curve truncated to 9 decimal places.

'''


import math
def len_curve(n):
    lst=[0]
    lstRes=[]
    res=0
    for i in range(n+1):
        lst.append(float(i)/n)
    for i in lst:
        lstRes.append(i**2)
    for i in range(len(lst)-1):
        res+=math.sqrt(((lst[i+1]-lst[i])**2)+((lstRes[i+1]-lstRes[i])**2))
    res=float("%.9f"%res)
    return res