import unittest
from parabolicArcLength import *

def test(self):
    self.assertEquals(len_curve(1), 1.414213562)
    self.assertEquals(len_curve(10), 1.478197397)

def main():
    unittest.main()

if __name__ == '__main__':
    main()