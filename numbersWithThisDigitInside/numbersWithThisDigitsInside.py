#!/usr/bin/python
# -*- coding: UTF-8 -*-

'''You have to search all numbers from inclusive 1 to inclusive a given number x, that have the given digit d in it.
The value of d will always be 0 - 9.
The value of x will always be greater than 0.

You have to return as an array

the count of these numbers,

their sum 

and their product.'''



import numpy
def numbers_with_digit_inside(x, d):
    listOfNumbers=[]
    for num in range(1,x+1):
        if str(d) in str(num):
            listOfNumbers.append(num)
    summ=0
    if len(listOfNumbers)>0:
        product=1
    else:
        product=0
    for i in listOfNumbers:
        summ+=i
        product*=i
    return [len(listOfNumbers), summ, product]