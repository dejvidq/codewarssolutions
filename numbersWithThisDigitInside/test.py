import unittest
from numbersWithDigitInside import *

def test(self):
    self.assertEqual((5, 6), [0, 0, 0])
    self.assertEqual((7, 6), [1, 6, 6])
    self.assertEqual((11, 1), [3, 22, 110])
    self.assertEqual((20, 0), [2, 30, 200])
    self.assertEqual((44, 4), [9, 286, 5955146588160])


def main():
    unittest.main()

if __name__ == '__main__':
    main()