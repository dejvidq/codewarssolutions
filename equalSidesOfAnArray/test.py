import unittest
from equalSidesOfAnArray import *

def test(self):
    self.assertEquals(find_even_index([1,2,3,4,3,2,1]),3)
    self.assertEquals(find_even_index([1,100,50,-51,1,1]),1,)
    self.assertEquals(find_even_index([1,2,3,4,5,6]),-1)
    self.assertEquals(find_even_index([20,10,30,10,10,15,35]),3)
    self.assertEquals(find_even_index([20,10,-80,10,10,15,35]),0)
    self.assertEquals(find_even_index([10,-80,10,10,15,35,20]),6)
    self.assertEquals(find_even_index(range(1,100)),-1)
    self.assertEquals(find_even_index([0,0,0,0,0]),0)
    self.assertEquals(find_even_index([-1,-2,-3,-4,-3,-2,-1]),3)
    self.assertEquals(find_even_index(range(-100,-1)),-1)

def main():
    unittest.main()

if __name__ == '__main__':
    main()