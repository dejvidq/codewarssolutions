import unittest
from partialWordSearching import *

def test(self):
    self.assertEqual(word_search("ab", ["za", "ab", "abc", "zab", "zbc"]), ["ab", "abc", "zab"])

    
def main():
    unittest.main()

if __name__ == '__main__':
    main()
