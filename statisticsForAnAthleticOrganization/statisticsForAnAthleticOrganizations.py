#!/usr/bin/python
# -*- coding: UTF-8 -*-


#You are the "computer man" of a local Athletic Association (C.A.A.). Many teams of runners come to compete. Each time you get a string of all #race results of every team who has run. For example here is a string showing the individual results of a team of 5:
#
#"01|15|59, 1|47|6, 01|17|20, 1|32|34, 2|3|17"
#
#Each part of the string is of the form: h|m|s where h, m, s are positive or null integer (represented as strings) with one or two digits. #There are no traps in this format.
#
#To compare the results of the teams you are asked for giving three statistics: range, average and median.
#
#Range : difference between the lowest and highest values. In {4, 6, 9, 3, 7} the lowest value is 3, and the highest is 9, so the range is 9 − #3 = 6.
#
#Mean or Average : To calculate mean, add together all of the numbers in a set and then divide the sum by the total count of numbers.
#
#Median : In statistics, the median is the number separating the higher half of a data sample from the lower half. The median of a finite list #of numbers can be found by arranging all the observations from lowest value to highest value and picking the middle one (e.g., the median of 
#{3, 3, 5, 9, 11} is 5) when there is an odd number of observations. If there is an even number of observations, then there is no single middle #value; the median is then defined to be the mean of the two middle values (the median of {3, 5, 6, 9} is (5 + 6) / 2 = 5.5).
#
#Your task is to return a string giving these 3 values. For the example given above, the string result will be
#
#"Range: 00|47|18 Average: 01|35|15 Median: 01|32|34"
#
#of the form:
#
#"Range: hh|mm|ss Average: hh|mm|ss Median: hh|mm|ss"
#
#where hh, mm, ss are integers (represented by strings) with each 2 digits.
#
#Remarks:
#
#if a result in seconds is ab.xy... it will be given truncated as ab.
#if the given string is "" you will return ""



def stat(strg):
    lst=strg.split(',')
    lst2=[]
    for i in lst:
        lst2.append(i.split('|'))
    lst3=[]
    try:
        for j in lst2:
            lst3.append(int(j[0])*3600+int(j[1])*60+int(j[2]))
    except ValueError:
        pass
	print lst3
    lst4=sorted(lst3)
    try:
        rng=lst4[-1]-lst4[0]
    except IndexError:
        rng=0
    try:
        avg=sum(lst4)/len(lst4)
    except ZeroDivisionError:
        avg=0
    try:
        if len(lst4)%2==0:
            med=(lst4[len(lst3)/2-1]+lst4[(len(lst3)/2)])/2
        else:
            med=lst4[(len(lst4)/2)]
    except IndexError:
        med=0
    h1,h2,h3,m1,m2,m3,s1,s2,s3=0,0,0,0,0,0,0,0,0
    h1=rng/3600
    m1=(rng-h1*3600)/60
    s1=rng-m1*60-h1*3600
    h2=avg/3600
    m2=(avg-h2*3600)/60
    s2=avg-m2*60-h2*3600
    h3=med/3600
    m3=(med-h3*3600)/60
    s3=med-m3*60-h3*3600
    if h1<10:
        h1="0"+str(h1)
    if h2<10:
        h2="0"+str(h2)
    if h3<10:
        h3="0"+str(h3)
    if m1<10:
        m1="0"+str(m1)
    if m2<10:
        m2="0"+str(m2)
    if m3<10:
        m3="0"+str(m3)
    if s1<10:
        s1="0"+str(s1)
    if s2<10:
        s2="0"+str(s2)
    if s3<10:
        s3="0"+str(s3)
    res="Range: %s|%s|%s Average: %s|%s|%s Median: %s|%s|%s" % (h1,m1,s1,h2,m2,s2,h3,m3,s3)
    return res