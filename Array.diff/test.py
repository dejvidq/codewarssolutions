import unittest
from array_diff import *

class Tests(unittest.TestCase):
    def test(self):
        self.assertEqual(array_diff([1,2], [1]), [2])
        self.assertEqual(array_diff([1,2,2], [1]), [2,2])
        self.assertEqual(array_diff([1,2,2], [2]), [1])
        self.assertEqual(array_diff([1,2,2], []), [1,2,2])
        self.assertEqual(array_diff([], [1,2]), [])
        
def main():
    unittest.main()

if __name__ == '__main__':
    main()
