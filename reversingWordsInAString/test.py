import unittest
from reversingWordInAString import *


def test(self):
	self.assertEqual(reverse('Hello World'), 'World Hello')
    self.assertEqual(reverse('Hi There.'), 'There. Hi')

def main():
    unittest.main()

if __name__ == '__main__':
    main()