import unittest
from regexValidatePinCode import *

def test(self):
	self.assertEqual(validate_pin("1"),False)
	self.assertEqual(validate_pin("12"),False)
	self.assertEqual(validate_pin("a234"),False)
	self.assertEqual(validate_pin(".234"),False)
	self.assertEqual(validate_pin("1234"),True)
	self.assertEqual(validate_pin("0000"),True)

def main():
    unittest.main()

if __name__ == '__main__':
    main()