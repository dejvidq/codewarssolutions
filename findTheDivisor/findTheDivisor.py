#!/usr/bin/python
# -*- coding: UTF-8 -*-

'''Create a function named divisors that takes an integer and returns an array with all of the integer's 
divisors(except for 1 and the number itself). If the number is prime return the string '(integer) is prime' 
(use Either String a in Haskell).

Example:

divisors(12); #should return [2,3,4,6]
divisors(25); #should return [5]
divisors(13); #should return "13 is prime"
You can assume that you will only get positive integers as inputs.'''

def divisors(integer):
    lst=[]
    for i in range(2,int(integer**0.5)+1):
        if integer%i==0:
            lst.append(i)
            lst.append(integer/i)
    lst.sort()
    if len(lst)==0:
        return "%d is prime" % integer
    else:
        return lst