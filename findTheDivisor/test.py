import unittest
from findTheDivisor import *

def test(self):
    self.assertEquals(divisors(15), [3, 5]);
    self.assertEquals(divisors(12), [2, 3, 4, 6]);
    self.assertEquals(divisors(13), "13 is prime");

def main():
    unittest.main()

if __name__ == '__main__':
    main()