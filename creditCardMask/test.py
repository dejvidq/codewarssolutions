import unittest
from creditCardMask import *

def test(self):
    self.assertEqual(maskify('aW3D4fg5'), '####4fg5')
    self.assertEqual(maskify(''),'')
    self.assertEqual(maskify('12'),'12')

def main():
    unittest.main()

if __name__ == '__main__':
    main()