'''You are given an array (which will have a length of at least 3, but could be very large) containing integers. The array is either entirely comprised of odd integers or entirely comprised of even integers except for a single integer N. Write a method that takes the array as an argument and returns N.

For example:

[2, 4, 0, 100, 4, 11, 2602, 36]

Should return: 11

[160, 3, 1719, 19, 11, 13, -21]

Should return: 160'''

def find_outlier(integers):
    even = 0
    for i in integers:
        if i%2==0:
            even+=1
    if even>1:
        z = [x for x in integers if x%2==1]
    else:
        z = [x for x in integers if x%2==0]
    return z[0]
