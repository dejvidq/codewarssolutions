import unittest
from findTheParityOutlier import *

def test(self):
    self.assertEqual(find_outlier([2,6,8,10,3]), 3)
    self.assertEqual(find_outlier([0,4,12,1,6]), 1)
    self.assertEqual(find_outlier([1,5,11,2,7]), 2)

def main():
    unittest.main()

if __name__ == '__main__':
    main()