import unittest
from binaryAddition import *

def test(self):
	self.assertEqual(add_binary(1,1),"10")
	self.assertEqual(add_binary(0,1),"1")
	self.assertEqual(add_binary(1,0),"1")
	self.assertEqual(add_binary(2,2),"100")
	self.assertEqual(add_binary(51,12),"111111")

def main():
    unittest.main()

if __name__ == '__main__':
    main()