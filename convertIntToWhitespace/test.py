import unittest
from convertIntToWhitespace import *

def unbleach(ws):
    return ws.replace(' ', '[space]').replace('\t', '[tab]').replace('\n', '[LF]')

def test(self):
	self.assertEqual(unbleach(whitespace_number( 1)), unbleach(   " \t\n"))
	self.assertEqual(unbleach(whitespace_number( 0)), unbleach(     " \n"))
	self.assertEqual(unbleach(whitespace_number(-1)), unbleach(  "\t\t\n"))
	self.assertEqual(unbleach(whitespace_number( 2)), unbleach(  " \t \n"))
	self.assertEqual(unbleach(whitespace_number(-3)), unbleach("\t\t\t\n"))

def main():
    unittest.main()

if __name__ == '__main__':
    main()