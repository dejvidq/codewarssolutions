#!/usr/bin/python
# -*- coding: UTF-8 -*-

'''Write an algorithm that will identify valid IPv4 addresses in dot-decimal format. 
Input to the function is guaranteed to be a single string.

Examples of valid inputs: 1.2.3.4 123.45.67.89

Examples of invalid inputs: 1.2.3 1.2.3.4.5 123.456.78.90 123.045.067.089'''



def is_valid_IP(strng):
    str = strng.split('.')
    if len(str)!=4:
        return False
    for i in str:
        for c in i:
            if c.isalpha():
                return False
    for i in str:
        if int(i)<0 or int(i)>255 or " " in i or i.startswith('0'):
            return False
    return True