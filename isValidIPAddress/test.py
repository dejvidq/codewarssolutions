import unittest
from isValidIP import *

class Test(unittest.TestCase):
    def test(self):
        self.assertEqual(is_valid_IP('12.255.56.1'),     True)
        self.assertEqual(is_valid_IP(''),                False)
        self.assertEqual(is_valid_IP('abc.def.ghi.jkl'), False)
        self.assertEqual(is_valid_IP('123.456.789.0'),   False)
        self.assertEqual(is_valid_IP('12.34.56'),        False)
        self.assertEqual(is_valid_IP('12.34.56 .1'),     False)
        self.assertEqual(is_valid_IP('12.34.56.-1'),     False)
        self.assertEqual(is_valid_IP('123.045.067.089'), False)

def main():
    unittest.main()

if __name__ == '__main__':
    main()
