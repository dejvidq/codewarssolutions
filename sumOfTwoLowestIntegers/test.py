import unittest
from sumOfTwoLowestIntegers import *

def test(self):
	self.assertEquals(sum_two_smallest_numbers([5, 8, 12, 18, 22]), 13)
	self.assertEquals(sum_two_smallest_numbers([7, 15, 12, 18, 22]), 19)
	self.assertEquals(sum_two_smallest_numbers([25, 42, 12, 18, 22]), 30)
	self.assertEquals(sum_two_smallest_numbers([1, 8, 12, 18, -1]), 0)
	self.assertEquals(sum_two_smallest_numbers([-1, -1]), -2)

def main():
    unittest.main()

if __name__ == '__main__':
    main()