'''Description:

There's a 3 for 2 offer on mangoes. For a given price and quantity, calculate the total cost of the mangoes.'''



def mango(x, price):
    return (x*price-(x//3)*price)