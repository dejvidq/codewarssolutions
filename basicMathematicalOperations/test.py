import unttest
from basicMathematicalOperations import *

def test(self):
	self.assertEquals(basic_op('+', 4, 7), 11)
	self.assertEquals(basic_op('-', 15, 18), -3)
	self.assertEquals(basic_op('*', 5, 5), 25)
	self.assertEquals(basic_op('/', 49, 7), 7)

def main():
    unittest.main()

if __name__ == '__main__':
    main()