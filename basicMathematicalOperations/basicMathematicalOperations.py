#!/usr/bin/python
# -*- coding: UTF-8 -*-

'''Your task is to create a function - basicOp().

The function should take three arguments - operation(string), value1(integer), value2(integer). The function should return result of integers after applying chosen operation.

Examples: 

basic_op('+', 4, 7) # Output: 11 
basic_op('-', 15, 18) # Output: -3 
basic_op('*', 5, 5) # Output: 25 
basic_op('/', 49, 7) # Output: 7 
'''

def basic_op(operator, value1, value2):
    return eval(str(value1)+operator+str(value2))